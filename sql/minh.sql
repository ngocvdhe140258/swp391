USE [projectSP23]
GO
/****** Object:  Table [dbo].[Accounts]    Script Date: 3/21/2023 4:10:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Accounts](
	[accountID] [int] IDENTITY(1,1) NOT NULL,
	[userName] [nvarchar](200) NOT NULL,
	[password] [nvarchar](200) NOT NULL,
	[email] [nvarchar](255) NULL,
	[role] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[accountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 3/21/2023 4:10:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[categoryID] [int] IDENTITY(1,1) NOT NULL,
	[cid] [int] NULL,
	[categoryName] [nvarchar](255) NOT NULL,
	[describe] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[categoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderDetail]    Script Date: 3/21/2023 4:10:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetail](
	[orderID] [int] NOT NULL,
	[productID] [int] NOT NULL,
	[quantity] [int] NULL,
	[price] [money] NULL,
PRIMARY KEY CLUSTERED 
(
	[orderID] ASC,
	[productID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 3/21/2023 4:10:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[orderID] [int] IDENTITY(1,1) NOT NULL,
	[totalmoney] [money] NULL,
	[createAt] [date] NULL,
	[steam] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[orderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 3/21/2023 4:10:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[productID] [int] IDENTITY(1,1) NOT NULL,
	[categoryID] [int] NULL,
	[productName] [nvarchar](255) NOT NULL,
	[image] [nvarchar](255) NULL,
	[price] [money] NULL,
	[quantity] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[productID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Profiles]    Script Date: 3/21/2023 4:10:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Profiles](
	[accountID] [int] NOT NULL,
	[firstName] [nvarchar](50) NULL,
	[lastName] [nvarchar](50) NULL,
	[avatar] [nvarchar](200) NULL,
	[country] [nvarchar](150) NULL,
	[address] [nvarchar](150) NULL,
	[birthday] [datetime] NULL,
	[phone] [nvarchar](15) NULL,
	[gender] [bit] NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Accounts] ON 

INSERT [dbo].[Accounts] ([accountID], [userName], [password], [email], [role]) VALUES (1, N'admin', N'123', N'test123@gmail.com', 1)
INSERT [dbo].[Accounts] ([accountID], [userName], [password], [email], [role]) VALUES (2, N'minh', N'minhyt123', N'minhtrn001@gmail.com', 1)
INSERT [dbo].[Accounts] ([accountID], [userName], [password], [email], [role]) VALUES (3, N'sa', N'12345', N'sa@gmail.com', 1)
INSERT [dbo].[Accounts] ([accountID], [userName], [password], [email], [role]) VALUES (4, N'nam', N'123', N'hai', 0)
INSERT [dbo].[Accounts] ([accountID], [userName], [password], [email], [role]) VALUES (5, N'haithan', N'minh', N'minhyt@gmail.com', 0)
INSERT [dbo].[Accounts] ([accountID], [userName], [password], [email], [role]) VALUES (6, N'abc', N'minh', N'minh', 0)
SET IDENTITY_INSERT [dbo].[Accounts] OFF
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([categoryID], [cid], [categoryName], [describe]) VALUES (1, 1, N'Knife', NULL)
INSERT [dbo].[Categories] ([categoryID], [cid], [categoryName], [describe]) VALUES (2, 2, N'Gun', NULL)
INSERT [dbo].[Categories] ([categoryID], [cid], [categoryName], [describe]) VALUES (3, 3, N'Gloves', NULL)
INSERT [dbo].[Categories] ([categoryID], [cid], [categoryName], [describe]) VALUES (4, 4, N'Containers', NULL)
INSERT [dbo].[Categories] ([categoryID], [cid], [categoryName], [describe]) VALUES (5, 5, N'Sticker', NULL)
INSERT [dbo].[Categories] ([categoryID], [cid], [categoryName], [describe]) VALUES (6, 6, N'Agent', NULL)
INSERT [dbo].[Categories] ([categoryID], [cid], [categoryName], [describe]) VALUES (7, 1, N'Butterfly', N'Knife')
INSERT [dbo].[Categories] ([categoryID], [cid], [categoryName], [describe]) VALUES (8, 1, N'Talon', N'Knife')
INSERT [dbo].[Categories] ([categoryID], [cid], [categoryName], [describe]) VALUES (9, 1, N'Flip', N'Knife')
INSERT [dbo].[Categories] ([categoryID], [cid], [categoryName], [describe]) VALUES (10, 1, N'M9 Bayonet', N'Knife')
INSERT [dbo].[Categories] ([categoryID], [cid], [categoryName], [describe]) VALUES (11, 1, N'Karambit', N'Knife')
INSERT [dbo].[Categories] ([categoryID], [cid], [categoryName], [describe]) VALUES (12, 2, N'AK-47', N'Gun')
INSERT [dbo].[Categories] ([categoryID], [cid], [categoryName], [describe]) VALUES (13, 2, N'M4A4', N'Gun')
INSERT [dbo].[Categories] ([categoryID], [cid], [categoryName], [describe]) VALUES (14, 2, N'M4A1S', N'Gun')
INSERT [dbo].[Categories] ([categoryID], [cid], [categoryName], [describe]) VALUES (15, 2, N'AWP', N'Gun')
INSERT [dbo].[Categories] ([categoryID], [cid], [categoryName], [describe]) VALUES (16, 2, N'FAMAS', N'Gun')
INSERT [dbo].[Categories] ([categoryID], [cid], [categoryName], [describe]) VALUES (17, 3, N'Glove', N'Gloves')
INSERT [dbo].[Categories] ([categoryID], [cid], [categoryName], [describe]) VALUES (18, 4, N'Case', N'Containers')
INSERT [dbo].[Categories] ([categoryID], [cid], [categoryName], [describe]) VALUES (19, 5, N'Sticker', N'Sticker')
INSERT [dbo].[Categories] ([categoryID], [cid], [categoryName], [describe]) VALUES (20, 6, N'Agent', N'Agent')
SET IDENTITY_INSERT [dbo].[Categories] OFF
GO
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (1, 7, N'Gamma Doppler (Minimal Wear)', N'usercss/images/knife/butterfly/butterflygammar.png', 17358.8700, 3)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (2, 7, N'Doppler Sapphire (Factory New)', N'usercss/images/knife/butterfly/butterflysapphire.png', 14900.0000, 1)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (3, 7, N'Crimson Web (Factory New)', N'usercss/images/knife/butterfly/crimsonweb.png', 6776.8900, 1)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (4, 7, N'Lore (Minimal Wear)', N'usercss/images/knife/butterfly/lore.png', 3961.3600, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (5, 7, N'Doppler Phase 4 (Factory New)', N'usercss/images/knife/butterfly/phase4.png', 4002.6900, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (6, 7, N'Doppler Ruby (Factory New)', N'usercss/images/knife/butterfly/ruby.png', 7950.0000, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (7, 7, N'Marble Fade (Factory New)', N'usercss/images/knife/butterfly/marblefade.png', 2621.6000, 3)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (8, 7, N'Autotronic (Minimal Wear)', N'usercss/images/knife/butterfly/autotronic.png', 1679.0000, 4)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (9, 8, N'Crimson Web (Field-Tested)', N'usercss/images/knife/talon/taloncrimson.png', 607.6600, 3)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (10, 8, N'Marble Fade (Minimal Wear)', N'usercss/images/knife/talon/talonmarblefade.png', 786.0000, 3)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (11, 8, N'Ultraviolet (Minimal Wear)', N'usercss/images/knife/talon/ultraviolet.png', 595.9000, 3)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (12, 8, N'Case Hardened (Factory New)', N'usercss/images/knife/talon/taloncase.png', 595.9000, 3)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (13, 8, N'Slaughter (Minimal Wear)', N'usercss/images/knife/talon/slaughter.png', 699.9900, 1)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (14, 8, N'Tiger Tooth (Factory New)', N'usercss/images/knife/talon/tigertooth.png', 663.1900, 1)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (15, 9, N'Marble Fade (Factory New)', N'usercss/images/knife/flip/flipmarblefade.png', 3968.5800, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (16, 9, N'Gamma Doppler Emerald (Minimal Wear)', N'usercss/images/knife/flip/flipemerald.png', 2783.9700, 1)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (17, 9, N'Doppler Sapphire (Factory New)', N'usercss/images/knife/flip/flipsapphire.png', 3199.0000, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (18, 9, N'Doppler Ruby (Minimal Wear)', N'usercss/images/knife/flip/flipruby.png', 2265.8100, 1)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (19, 9, N'Black Pearl (Factory New)', N'usercss/images/knife/flip/flipblackpearl.png', 1278.3100, 1)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (20, 9, N'Fade (Factory New)', N'usercss/images/knife/flip/flipfade.png', 729.5900, 3)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (21, 10, N'Crimson Web (Factory New)', N'usercss/images/knife/m9bayonet/m9crimson.png', 15839.6500, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (22, 10, N'Doppler Sapphire (Minimal Wear)', N'usercss/images/knife/m9bayonet/m9sapphire.png', 10816.6900, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (23, 10, N'Doppler Phase 4 (Minimal Wear)', N'usercss/images/knife/m9bayonet/m9dopperphase4.png', 1988.5500, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (24, 10, N'Lore (Factory New)', N'usercss/images/knife/m9bayonet/m9lore.png', 2850.0000, 3)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (25, 10, N'Doppler Phase 2 (Factory New)', N'usercss/images/knife/m9bayonet/m9dopperphase2.png', 1442.5100, 1)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (26, 10, N'Autotronic (Minimal Wear)', N'usercss/images/knife/m9bayonet/m9autotronic.png', 1242.6000, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (27, 11, N'Marble Fade (Factory New)', N'usercss/images/knife/karambit/karammarblefade.png', 2590.0000, 4)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (28, 11, N'Doppler Phase 3 (Factory New)', N'usercss/images/knife/karambit/karamdopperphase3.png', 1481.1500, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (29, 11, N'Doppler Phase 2 (Factory New)', N'usercss/images/knife/karambit/karamdopperphase2.png', 1208.0000, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (30, 11, N'Slaughter (Factory New)', N'usercss/images/knife/karambit/karamslaughter.png', 1208.0000, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (31, 11, N'Tiger Tooth (Factory New)', N'usercss/images/knife/karambit/karamtigertooth.png', 1030.9300, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (32, 11, N'Black Laminate (Factory New)', N'usercss/images/knife/karambit/karamblacklaminate.png', 1142.0000, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (33, 12, N'Wild Lotus (Field-Tested)', N'usercss/images/gun/ak47/akwildlotus.png', 6499.0000, 3)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (34, 12, N'Fire Serpent (Factory New)', N'usercss/images/gun/ak47/akfireserpent.png', 5599.0000, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (35, 12, N'Vulcan (Factory New)', N'usercss/images/gun/ak47/akvulcan.png', 764.8500, 4)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (36, 12, N'Case Hardened (Factory New)', N'usercss/images/gun/ak47/akcase.png', 1099.9900, 1)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (37, 12, N'Asiimov (Factory New)', N'usercss/images/gun/ak47/akasiimov.png', 1049.0000, 3)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (38, 13, N'Howl (Factory New)', N'usercss/images/gun/m4a4/m4a4howl.png', 17175.3100, 3)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (39, 13, N'Poseidon (Minimal Wear)', N'usercss/images/gun/m4a4/m4a4poseidon.png', 1092.3400, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (40, 13, N'The Emperor (Factory New)', N'usercss/images/gun/m4a4/m4a4theemperor.png', 824.9900, 4)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (41, 13, N'Temukau (Minimal Wear)', N'usercss/images/gun/m4a4/m4a4temukau.png', 374.0000, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (42, 13, N'Royal Paladin (Factory New)', N'usercss/images/gun/m4a4/m4a4paladin.png', 450.0000, 3)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (43, 14, N'Welcome to the Jungle', N'usercss/images/gun/m4a1s/m4a1sjungle.png', 1037.0000, 3)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (44, 14, N'Master Piece (Factory New)', N'usercss/images/gun/m4a1s/m4a1smaster.png', 1074.4200, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (45, 14, N'Hot Rod (Factory New)', N'usercss/images/gun/m4a1s/m4a1shotrod.png', 472.0000, 5)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (46, 14, N'Icarus Fell (Factory New)', N'usercss/images/gun/m4a1s/m4a1sicarus.png', 364.4400, 1)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (47, 14, N'Printstream (Minimal Wear)', N'usercss/images/gun/m4a1s/m4a1sprintstream.png', 391.4000, 3)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (48, 15, N'Gungnir (Factory New)', N'usercss/images/gun/awp/awpgungnir.png', 9999.0000, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (49, 15, N'Dragon Lore (Field-Tested)', N'usercss/images/gun/awp/awpdragonlore.png', 5500.0000, 4)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (50, 15, N'Medusa (Field-Tested)', N'usercss/images/gun/awp/awpmedusa.png', 1927.2500, 3)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (51, 15, N'Fade (Factory New)', N'usercss/images/gun/awp/awpfade.png', 847.1200, 3)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (52, 15, N'Oni Taiji (Minimal Wear)', N'usercss/images/gun/awp/awponi.png', 499.9900, 4)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (53, 16, N'Teardown (Factory New)', N'usercss/images/gun/famas/famasteardown.png', 499.9800, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (54, 16, N'Commemoration (Factory New)', N'usercss/images/gun/famas/famascomme.png', 62.7300, 3)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (55, 16, N'Roll Cage (Factory New)', N'usercss/images/gun/famas/famasrollcage.png', 499.9800, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (56, 16, N'Meltdown (Factory New)', N'usercss/images/gun/famas/famasmeltdown.png', 17.8700, 5)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (57, 16, N'Styx (Minimal Wear)', N'usercss/images/gun/famas/famasstyx.png', 48.2100, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (58, 17, N'Amphibious (Factory New)', N'usercss/images/gloves/amphibious.png', 6443.0800, 3)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (59, 17, N'Hedge Maze (Minimal Wear)', N'usercss/images/gloves/hedgemaze.png', 7366.8900, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (60, 17, N'Marble Fade (Factory New)', N'usercss/images/gloves/mablefade.png', 5328.9600, 4)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (61, 17, N'Black Tie (Factory New)', N'usercss/images/gloves/blacktie.png', 3888.4600, 1)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (62, 17, N'Blood Pressure (Factory New)', N'usercss/images/gloves/blood.png', 4441.2400, 3)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (63, 18, N'CS:GO Weapon Case', N'usercss/images/case/csgoweapon.png', 71.8900, 100)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (64, 18, N'Snakebite Case', N'usercss/images/case/snakebite.png', 0.2400, 500)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (65, 18, N'Clutch Case', N'usercss/images/case/clutch.png', 0.5800, 1000)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (66, 18, N'Glove Case', N'usercss/images/case/glovecase.png', 3.2000, 500)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (67, 18, N'Stockholm 2021 Viewer Pass', N'usercss/images/case/stockholm.png', 23.3200, 100)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (68, 19, N'Team LDLC.com (Holo)', N'usercss/images/sticker/teamldlcholo.png', 20567.3800, 2)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (69, 19, N'Natus Vincere (Holo)', N'usercss/images/sticker/navi2014.png', 9999.0000, 1)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (70, 19, N'3DMAX (Holo)', N'usercss/images/sticker/3dmax2014.png', 6630.9600, 1)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (71, 19, N'iBUYPOWER', N'usercss/images/sticker/ibuypower2014.png', 3995.0000, 4)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (72, 19, N'Ninjas in Pyjamas (Holo)', N'usercss/images/sticker/nip2014.png', 2000.0000, 3)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (73, 20, N'The Professionals', N'usercss/images/agent/theprofessionals.png', 48.5000, 20)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (74, 20, N'SEAL Frogman', N'usercss/images/agent/seal.png', 37.4900, 30)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (75, 20, N'Getaway Sally', N'usercss/images/agent/getaway.png', 34.8000, 50)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (76, 20, N'Number K', N'usercss/images/agent/numberK.png', 29.9900, 32)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (77, 20, N'Sir Bloody Loudmouth Darryl', N'usercss/images/agent/sirbloody.png', 22.8800, 55)
INSERT [dbo].[Products] ([productID], [categoryID], [productName], [image], [price], [quantity]) VALUES (83, NULL, N'AK-46', N'https://www.elecspo.com/static/uploads/13/2018/10/ak47frontsidemisty.jpg', 200.0000, 4)
SET IDENTITY_INSERT [dbo].[Products] OFF
GO
INSERT [dbo].[Profiles] ([accountID], [firstName], [lastName], [avatar], [country], [address], [birthday], [phone], [gender]) VALUES (1, N'FirstName', N'LastName', NULL, N'Việt Nam', N'Hà Nội', CAST(N'2002-02-02T00:00:00.000' AS DateTime), N'0123456789', 1)
INSERT [dbo].[Profiles] ([accountID], [firstName], [lastName], [avatar], [country], [address], [birthday], [phone], [gender]) VALUES (2, N'Minh', N'Trần', N'https://fandom.vn/wp-content/uploads/2019/04/naruto-namikaze-minato-1.jpg', N'Việt Nam', N'Hà Nội', CAST(N'2001-12-14T00:00:00.000' AS DateTime), N'0398665357', 1)
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Accounts__66DCF95CD4A7DF71]    Script Date: 3/21/2023 4:10:34 PM ******/
ALTER TABLE [dbo].[Accounts] ADD UNIQUE NONCLUSTERED 
(
	[userName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [UQ__Profiles__F267253FF38E5C44]    Script Date: 3/21/2023 4:10:34 PM ******/
ALTER TABLE [dbo].[Profiles] ADD UNIQUE NONCLUSTERED 
(
	[accountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Orders] ADD  DEFAULT (getdate()) FOR [createAt]
GO
ALTER TABLE [dbo].[Categories]  WITH CHECK ADD  CONSTRAINT [pk_categories_categories] FOREIGN KEY([cid])
REFERENCES [dbo].[Categories] ([categoryID])
GO
ALTER TABLE [dbo].[Categories] CHECK CONSTRAINT [pk_categories_categories]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [fk_orderLine_order] FOREIGN KEY([orderID])
REFERENCES [dbo].[Orders] ([orderID])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [fk_orderLine_order]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [fk_product_orderLine] FOREIGN KEY([productID])
REFERENCES [dbo].[Products] ([productID])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [fk_product_orderLine]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [fk_product_category] FOREIGN KEY([categoryID])
REFERENCES [dbo].[Categories] ([categoryID])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [fk_product_category]
GO
