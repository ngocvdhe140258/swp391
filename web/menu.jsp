<%-- 
    Document   : menu
    Created on : Mar 11, 2023, 7:13:04 PM
    Author     : acer
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- HEADER -->
<header>
    <!-- TOP HEADER -->
    <div id="top-header">
        <div class="container">
            <ul class="header-links pull-left">
                <li><a href="#"><i class="fa fa-phone"></i> +84 398665351</a></li>
                <li><a href="#"><i class="fa fa-envelope-o"></i> minhtrn001gmail.com</a></li>
                <li><a href="#"><i class="fa fa-map-marker"></i> FPT University</a></li>
            </ul>
            <ul class="header-links pull-right">
                <c:if test="${sessionScope.acc.role == 1}">
                    <li><a href="manager"><i class="fa fa-user-o"></i>Manager All Product</a></li>
                    <li><a href="#"><i class="fa fa-user-o"></i>Manager All Account</a></li>
                    <li><a href="profile?accountID=${sessionScope.acc.accountID}"><i class="fa fa-user-o"></i>Manager My Account</a></li>
                    </c:if>
                    <c:if test="${sessionScope.acc.role == 0}">
                    <li><a href="profile?accountID=${sessionScope.acc.accountID}"><i class="fa fa-user-o"></i>Manager My Account</a></li>
                    </c:if>

                <li><a href="#"><i class="fa fa-dollar"></i> USD</a></li>
                    <c:if test="${sessionScope.acc != null}">
                    <li><a href="logout"><i class="fa fa-user-o"></i> Logout(${sessionScope.acc.username})</a></li>
                    </c:if>
                    <c:if test="${sessionScope.acc == null}">
                    <li><a href="login.jsp"><i class="fa fa-user-o"></i> Login</a></li>
                    </c:if>
            </ul>
        </div>
    </div>
    <!-- /TOP HEADER -->

    <!-- MAIN HEADER -->
    <div id="header">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- LOGO -->
                <div class="col-md-3">
                    <div class="header-logo">
                        <a href="#" class="logo">
                            <img src="./img/logo.png" alt="">
                        </a>
                    </div>
                </div>
                <!-- /LOGO -->

                <!-- SEARCH BAR -->
                <div class="col-md-6">
                    <div class="header-search">
                        <form action="search" method="post">
                            <select class="input-select" name="searchID">
                                <option value="0">All Iteams</option>
                                <c:forEach items="${listC}" var="o">
                                    <option value="${o.categoryID}">${o.categoryName}</option>
                                </c:forEach>
                            </select>
                            <input class="input" placeholder="Search here" name="txt" type="text" value="${txtS}">
                            <button class="search-btn" type="submit">Search</button>
                        </form>
                    </div>
                </div>
                <!-- /SEARCH BAR -->

                <!-- ACCOUNT -->
                <div class="col-md-3 clearfix">
                    <div class="header-ctn">
                        <!-- Wishlist -->
                        <div>
                            <a href="#">
                                <i class="fa fa-heart-o"></i>
                                <span>Your Wishlist</span>
                                <div class="qty">2</div>
                            </a>
                        </div>
                        <!-- /Wishlist -->

                        <!-- Cart -->
                        <div class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                <i class="fa fa-shopping-cart"></i>
                                <span>Your Cart</span>
                                <div class="qty">3</div>
                            </a>
                            <div class="cart-dropdown">
                                <div class="cart-list">
                                    <c:forEach items="${listS}" var="o">
                                <div class="product-widget">
                                    <div class="product-img">
                                        <img src="${o.image}" alt="">
                                    </div>
                                    <div class="product-body">
                                        <p class="product-category">${o.categories.describe}</p>
                                        <h3 class="product-name"><a href="detail?pid=${o.productID}">${o.productName}</a></h3>
                                        <h4 class="product-price">${o.price}</h4>
                                    </div>
                                </div>

                                    </c:forEach>
                                </div>
                                <div class="cart-summary">
                                    <small>3 Item(s) selected</small>
                                    <h5>SUBTOTAL: $39035.76</h5>
                                </div>
                                <div class="cart-btns">
                                    <a href="#">View Cart</a>
                                    <a href="checkout.jsp">Checkout  <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>                         
                        <!-- /Cart -->

                        <!-- Menu Toogle -->
                        <div class="menu-toggle">
                            <a href="#">
                                <i class="fa fa-bars"></i>
                                <span>Menu</span>
                            </a>
                        </div>
                        <!-- /Menu Toogle -->
                    </div>
                </div>
                <!-- /ACCOUNT -->
            </div>
            <!-- row -->
        </div>
        <!-- container -->
    </div>
    <!-- /MAIN HEADER -->
</header>
<!-- /HEADER -->