/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import context.DBContext;
import entity.Account;
import entity.Category;
import entity.Product;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author acer
 */
public class DAO extends DBContext {

    public List<Product> getAllProduct() {
        List<Product> list = new ArrayList<>();
        String sql = "select p.productID,p.categoryID,p.productName,p.image,p.price,p.quantity,c.categoryName,c.cid,c.describe from Products p inner join Categories c on p.categoryID=c.categoryID";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category cate = new Category(rs.getInt(8), rs.getString(9), rs.getInt(8), rs.getString(9));
                list.add(new Product(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDouble(5),
                        rs.getInt(6),
                        cate));
            }
        } catch (Exception e) {
        }

        return list;
    }

    public List<Category> getAllCategory() {
        List<Category> list = new ArrayList<>();
        String sql = "select categoryID,categoryName,cid from Categories where categoryID between 1 and 6";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Category(rs.getInt(1), rs.getString(2), rs.getInt(3), null));
            }
        } catch (Exception e) {
        }

        return list;
    }

    public List<Product> getTopSellingProduct() {
        List<Product> list = new ArrayList<>();
        String sql = "select top 3 p.productID,p.categoryID,p.productName,p.image,p.price,p.quantity,c.categoryName,c.cid,c.describe from Products p inner join Categories c on p.categoryID=c.categoryID";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category cate = new Category(rs.getInt(2), rs.getString(7), rs.getInt(8), rs.getString(9));
                Product p = new Product();
                p.setProductID(rs.getInt(1));
                p.setCategories(cate);
                p.setProductName(rs.getString(3));
                p.setImage(rs.getString(4));
                p.setPrice(rs.getDouble(5));
                p.setQuantity(rs.getInt(6));
                list.add(p);
            }
        } catch (Exception e) {
        }

        return list;
    }

    public List<Product> getProDuctByCID(String cid) {
        List<Product> list = new ArrayList<>();
        String sql = "select p.productID,"
                + "p.categoryID,"
                + "p.productName,"
                + "p.image,p.price,"
                + "p.quantity,"
                + "c.categoryName,"
                + "c.cid,"
                + "c.describe "
                + "from Products p inner join Categories c "
                + "on p.categoryID=c.categoryID where cid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, cid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category cate = new Category(rs.getInt(2), rs.getString(7), rs.getInt(8), rs.getString(9));
                Product p = new Product();
                p.setProductID(rs.getInt(1));
                p.setCategories(cate);
                p.setProductName(rs.getString(3));
                p.setImage(rs.getString(4));
                p.setPrice(rs.getDouble(5));
                p.setQuantity(rs.getInt(6));
                list.add(p);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public Product getProDuctByID(String productID) {
        String sql = "select p.productID,"
                + "p.categoryID,"
                + "p.productName,"
                + "p.image,p.price,"
                + "p.quantity,"
                + "c.categoryName,"
                + "c.cid,"
                + "c.describe "
                + "from Products p inner join Categories c "
                + "on p.categoryID=c.categoryID where productID=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, productID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category cate = new Category(rs.getInt(2), rs.getString(7), rs.getInt(8), rs.getString(9));
                Product p = new Product();
                p.setProductID(rs.getInt(1));
                p.setCategories(cate);
                p.setProductName(rs.getString(3));
                p.setImage(rs.getString(4));
                p.setPrice(rs.getDouble(5));
                p.setQuantity(rs.getInt(6));
                return p;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public List<Product> searchProductByName(String txtSearch) {
        List<Product> list = new ArrayList<>();
        String sql = "select p.productID,p.categoryID,p.productName,p.image,p.price,p.quantity,c.categoryName,c.cid,c.describe from Products p inner join Categories c on p.categoryID=c.categoryID where productName like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + txtSearch + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category cate = new Category(rs.getInt(8), rs.getString(9), rs.getInt(8), rs.getString(9));
                list.add(new Product(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDouble(5),
                        rs.getInt(6),
                        cate));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public Account login(String username, String password) {
        String sql = "select * from Accounts where userName=? and [password] =?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            st.setString(2, password);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Account(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5));
            }
        } catch (Exception e) {
        }

        return null;
    }

    public Account checkAccountExist(String user) {
        String sql = "select * from Accounts where userName = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Account(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5));
            }
        } catch (Exception e) {
        }

        return null;
    }

    public void signup(String username, String password, String email) {
        String sql = "insert into Accounts values(?,?,?,0)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            st.setString(2, password);
            st.setString(3, email);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void deleteProduct(String pid) {
        String sql = "delete from Products where productID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, pid);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public List<Category> getCategoryChild() {
        List<Category> list = new ArrayList<>();
        String sql = "SELECT distinct  p.categoryID,c.categoryName,c.cid,c.describe from Products p inner join Categories c on p.categoryID=c.categoryID";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Category(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4)));
            }
        } catch (Exception e) {
        }

        return list;
    }

    public void addProduct(String productName, String image, String price, String quantity, String categoryID) {
        String sql = "INSERT INTO [dbo].[Products]([categoryID],[productName],[image],[price],[quantity])VALUES(?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, categoryID);
            st.setString(2, productName);
            st.setString(3, image);
            st.setString(4, price);
            st.setString(5, quantity);
            st.executeUpdate();
        } catch (Exception e) {
        }

    }

    public void editProduct(String productID, String categoryID, String productName, String image, String price, String quantity) {
        String sql = "update Products set [categoryID]=?,\n"
                + "[productName] = ?,\n"
                + "[image]= ?,\n"
                + "[price]= ?,\n"
                + "[quantity]= ?\n"
                + "where [productID]=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, categoryID);
            st.setString(2, productName);
            st.setString(3, image);
            st.setString(4, price);
            st.setString(5, quantity);
            st.setString(6, productID);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public int getTotalProduct() {
        String sql = "select count(*) from Products";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Product> getListByPage(List<Product> list,
            int start, int end) {
        ArrayList<Product> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public List<Product> searchProductByNameAndCategoryID(String txtSearch, String searchID) {
        List<Product> list = new ArrayList<>();
        String sql = "select p.productID,p.categoryID,p.productName,p.image,p.price,p.quantity,c.categoryName,c.cid,c.describe from Products p inner join Categories c on p.categoryID=c.categoryID where p.productName like ? and c.cid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + txtSearch + "%");
            st.setString(2, searchID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category cate = new Category(rs.getInt(8), rs.getString(9), rs.getInt(8), rs.getString(9));
                list.add(new Product(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDouble(5),
                        rs.getInt(6),
                        cate));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Product> searchProductByPrice(String price_min, String price_max) {
        List<Product> list = new ArrayList<>();
        String sql = "select p.productID,p.categoryID,p.productName,p.image,p.price,p.quantity,c.categoryName,c.cid,c.describe from Products p inner join Categories c on p.categoryID=c.categoryID where p.price between ? and ? order by p.price asc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, price_min);
            st.setString(2, price_max);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category cate = new Category(rs.getInt(8), rs.getString(9), rs.getInt(8), rs.getString(9));
                list.add(new Product(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDouble(5),
                        rs.getInt(6),
                        cate));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Product> sortProductByPriceMinToMax() {
        String sql = "select p.productID,p.categoryID,p.productName,p.image,p.price,p.quantity,c.categoryName,c.cid,c.describe from Products p inner join Categories c on p.categoryID=c.categoryID order by p.price asc";
        List<Product> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category cate = new Category(rs.getInt(8), rs.getString(9), rs.getInt(8), rs.getString(9));
                list.add(new Product(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDouble(5),
                        rs.getInt(6),
                        cate));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Product> sortProductByPriceMaxToMin() {
        String sql = "select p.productID,p.categoryID,p.productName,p.image,p.price,p.quantity,c.categoryName,c.cid,c.describe from Products p inner join Categories c on p.categoryID=c.categoryID order by p.price desc";
        List<Product> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category cate = new Category(rs.getInt(8), rs.getString(9), rs.getInt(8), rs.getString(9));
                list.add(new Product(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDouble(5),
                        rs.getInt(6),
                        cate));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Product> sortProductByName() {
        String sql = "select p.productID,p.categoryID,p.productName,p.image,p.price,p.quantity,c.categoryName,c.cid,c.describe from Products p inner join Categories c on p.categoryID=c.categoryID order by p.productName asc";
        List<Product> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category cate = new Category(rs.getInt(8), rs.getString(9), rs.getInt(8), rs.getString(9));
                list.add(new Product(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDouble(5),
                        rs.getInt(6),
                        cate));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public static void main(String[] args) {
        DAO dao = new DAO();
        List<Product> list = dao.searchProductByPrice("100", "1000");
        for (Product o : list) {
            System.out.println(o);
        }
    }
}
