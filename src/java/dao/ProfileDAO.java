/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import context.DBContext;
import entity.Account;
import entity.Profile;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author acer
 */
public class ProfileDAO extends DBContext{
    
    public List<Profile> getAllProfile(){
        List<Profile> list = new ArrayList<>();
        String sql="select a.accountID,"
                + "a.userName,"
                + "a.[password],"
                + " a.email,"
                + "a.[role],"
                + "p.firstName,"
                + "p.lastName,"
                + "p.avatar,"
                + "p.country,"
                + "p.[address],"
                + "p.birthday,"
                + "p.phone,"
                + "p.gender"
                + " from Accounts a inner join Profiles p "
                + "on a.accountID=p.accountID";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                Account ac = new Account(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5));
                list.add(new Profile(rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10),
                        rs.getDate(11),
                        rs.getString(12),
                        rs.getBoolean(13),
                        ac));
            }
        } catch (Exception e) {
        }
        return list;
    
    
}
    public Profile getProfileByID(String accountID) {
        String sql = "select a.accountID,"
                + "a.userName,"
                + "a.[password],"
                + " a.email,"
                + "a.[role],"
                + "p.firstName,"
                + "p.lastName,"
                + "p.avatar,"
                + "p.country,"
                + "p.[address],"
                + "p.birthday,"
                + "p.phone,"
                + "p.gender from Accounts a inner join Profiles p "
                + "on a.accountID=p.accountID where a.accountID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, accountID);
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                Profile p = new Profile();
                Account ac = new Account(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5));
                p.setFirstName(rs.getString(6));
                p.setLastName(rs.getString(7));
                p.setAvatar(rs.getString(8));
                p.setCountry(rs.getString(9));
                p.setAddress(rs.getString(10));
                p.setBirthday(rs.getDate(11));
                p.setPhone(rs.getString(12));
                p.setGender(rs.getBoolean(13));
                p.setAccount(ac);
                return p;
            }
        } catch (Exception e) {
        }
        return null;
    }
    
    public void editProfile(String accountID, String firstName, String lastName, String avatar, String country, String address, String birthday, String phone) {
        String sql = "update Profiles set [firstName] = ?, [lastName] = ?, [avatar]=?,[country]=?,[address]=?,[birthday]=?,[phone]=? where [accountID]=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, firstName);
            st.setString(2, lastName);
            st.setString(3, avatar);
            st.setString(4, country);
            st.setString(5, address);
            st.setString(6, birthday);
            st.setString(7, phone);
            st.setString(8, accountID);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }
    
    public static void main(String[] args) {
        ProfileDAO pdao = new ProfileDAO();
        Profile p = pdao.getProfileByID("2");
        System.out.println(p);
    }
}
