/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import entity.Order;

/**
 *
 * @author acer
 */
public class OrderDetail {

    /*orderID int,
            productID int,
	quantity int,
	price money*/
    private Order order;
    private int productID;
    private int quantity;
    private double price;

    public OrderDetail() {
    }

    public OrderDetail(Order order, int productID, int quantity, double price) {
        this.order = order;
        this.productID = productID;
        this.quantity = quantity;
        this.price = price;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
