/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.util.Date;
import entity.Account;

/**
 *
 * @author acer
 */
public class Profile {
    private String firstName;
    private String lastName;
    private String avatar;
    private String country;
    private String address;
    private Date birthday;
    private String phone;
    private boolean gender;
    private Account account;

    public Profile() {
    }

    public Profile(String firstName, String lastName, String avatar, String country, String address, Date birthday, String phone, boolean gender, Account account) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.avatar = avatar;
        this.country = country;
        this.address = address;
        this.birthday = birthday;
        this.phone = phone;
        this.gender = gender;
        this.account = account;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "Profile{" + "firstName=" + firstName + ", lastName=" + lastName + ", avatar=" + avatar + ", country=" + country + ", address=" + address + ", birthday=" + birthday + ", phone=" + phone + ", gender=" + gender + ", account=" + account + '}';
    }

    
    
    
    
}
