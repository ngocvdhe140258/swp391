/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author acer
 */
public class Cart {
    private List<Item> items;

    public Cart() {
        items = new ArrayList<>();
    }

    public List<Item> getItems() {
        return items;
    }
    public int getQuantityByID(int id){
        return getItemById(id).getQuantity();
    }
    
    private Item getItemById(int id){
        for (Item i : items) {
            if(i.getProduct().getProductID()==id){
                return i;
            }
        }
        return null;
    }
    
    public void addItem(Item t){
        if(getItemById(t.getProduct().getProductID())!= null){
            Item m = getItemById(t.getProduct().getProductID());
            m.setQuantity(m.getQuantity()+t.getQuantity());
        }else{
            items.add(t);
        }
    }
    
    public void removeItem(int id){
        if(getItemById(id)!= null){
            items.remove(getItemById(id));
        }
    }
    
    public double getTotalMoney(){
        double t=0;
        for (Item i : items) {
            t+=(i.getQuantity()*i.getPrice());
        }       
        return t;
    }
    private Product getProductById(int id, List<Product> list){
        for (Product p : list) {
            if(p.getProductID()==id){
                return p;
            }
        }
        return null;
    }
    public Cart(String ck, List<Product> list){
        items = new ArrayList<>();
        try{
        if(ck!=null && ck.length()!=0){
            String[] s = ck.split(",");
            for (String i : s) {
                String[] n = i.split(":");
                int id=Integer.parseInt(n[0]);
                int quantity=Integer.parseInt(n[1]);
                Product p=getProductById(id, list);
                Item t = new Item(p, quantity, p.getPrice()*2);
                addItem(t);
            }
        }
        }catch(NumberFormatException e){
            
        }
    }
}
