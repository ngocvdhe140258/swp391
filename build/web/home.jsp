<%-- 
    Document   : home
    Created on : Feb 28, 2023, 4:36:02 PM
    Author     : acer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		 <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

		<title>CSGO MARKET</title>

		<!-- Google font -->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

		<!-- Bootstrap -->
		<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>

		<!-- Slick -->
		<link type="text/css" rel="stylesheet" href="css/slick.css"/>
		<link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>

		<!-- nouislider -->
		<link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>

		<!-- Font Awesome Icon -->
		<link rel="stylesheet" href="css/font-awesome.min.css">

		<!-- Custom stlylesheet -->
		<link type="text/css" rel="stylesheet" href="css/style.css"/>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

    </head>
	<body>
            <jsp:include page="menu.jsp"></jsp:include>

		<!-- NAVIGATION -->
		<nav id="navigation">
			<!-- container -->
			<div class="container">
				<!-- responsive-nav -->
				<div id="responsive-nav">
					<!-- NAV -->
					<ul class="main-nav nav navbar-nav">
						<li class="active"><a href="#">Home</a></li>
						<li><a href="product">Store</a></li>
						<li><a href="category?cid=2">Gun</a></li>
						<li><a href="category?cid=1">Knife</a></li>
						<li><a href="category?cid=3">GLoves</a></li>
						<li><a href="category?cid=4">Containers</a></li>
						<li><a href="category?cid=5">Sticker</a></li>
						<li><a href="category?cid=6">Agents</a></li>
					</ul>
					<!-- /NAV -->
				</div>
				<!-- /responsive-nav -->
			</div>
			<!-- /container -->
		</nav>
		<!-- /NAVIGATION -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/dao.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Knife<br>Collection</h3>
								<a href="#" class="cta-btn">Shop now <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
					</div>
					<!-- /shop -->

					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/akasimod.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Gun<br>Collection</h3>
								<a href="#" class="cta-btn">Shop now <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
					</div>
					<!-- /shop -->

					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img style="height: 200px;" src="./img/gloves.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Gloves<br>Collection</h3>
								<a href="#" class="cta-btn">Shop now <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
					</div>
					<!-- /shop -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<!-- section title -->
					<div class="col-md-12">
						<div class="section-title">
							<h3 class="title">New Items</h3>
							<div class="section-nav">
								<ul class="section-tab-nav tab-nav">
									<li class="active"><a data-toggle="tab" href="#tab1">Knife</a></li>
									<li><a data-toggle="tab" href="#tab1">Gloves</a></li>
									<li><a data-toggle="tab" href="#tab1">Gun</a></li>
									<li><a data-toggle="tab" href="#tab1">Containers</a></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /section title -->

					<!-- Products tab & slick -->
					<div class="col-md-12">
						<div class="row">
							<div class="products-tabs">
								<!-- tab -->
								<div id="tab1" class="tab-pane active">
									<div class="products-slick" data-nav="#slick-nav-1">
										<!-- product -->
										<div class="product">
											<div class="product-img">
												<img src="./img/navaja.png" alt="">
												<div class="product-label">
													<span class="sale">-16%</span>
													<span class="new">NEW</span>
												</div>
											</div>
											<div class="product-body">
												<p class="product-category">Knife</p>
												<h3 class="product-name"><a href="#">★ Navaja Knife | Marble Fade (Factory New)</a></h3>
												<h4 class="product-price">$140.00 <del class="product-old-price">$150.00</del></h4>
												<div class="product-rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
												</div>
												<div class="product-btns">
													<button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button>
													<button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">add to compare</span></button>
													<button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
												</div>
											</div>
											<div class="add-to-cart">
												<button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
											</div>
										</div>
										<!-- /product -->

										<!-- product -->
										<div class="product">
											<div class="product-img">
												<img src="./img/gammardoppler.png" alt="">
												<div class="product-label">
													<span class="sale">-26.15%</span>
													<span class="new">NEW</span>
												</div>
											</div>
											<div class="product-body">
												<p class="product-category">Knife</p>
												<h3 class="product-name"><a href="#">StatTrak™ ★ Butterfly Knife / Minimal Wear
													Gamma Doppler Emerald (Minimal Wear)</a></h3>
												<h4 class="product-price">$23 506.80 <del class="product-old-price">$17 358.87</del></h4>
												<div class="product-rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-o"></i>
												</div>
												<div class="product-btns">
													<button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button>
													<button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">add to compare</span></button>
													<button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
												</div>
											</div>
											<div class="add-to-cart">
												<button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
											</div>
										</div>
										<!-- /product -->

										<!-- product -->
										<div class="product">
											<div class="product-img">
												<img src="./img/vucal.png" alt="">
												<div class="product-label">
													<span class="sale">-6.18%</span>
												</div>
											</div>
											<div class="product-body">
												<p class="product-category">AK-47</p>
												<h3 class="product-name"><a href="#">StatTrak™ AK-47 / Minimal Wear
													Vulcan (Minimal Wear)</a></h3>
												<h4 class="product-price">$900.00 <del class="product-old-price">$959.00</del></h4>
												<div class="product-rating">
												</div>
												<div class="product-btns">
													<button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button>
													<button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">add to compare</span></button>
													<button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
												</div>
											</div>
											<div class="add-to-cart">
												<button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
											</div>
										</div>
										<!-- /product -->

										<!-- product -->
										<div class="product">
											<div class="product-img">
												<img src="./img/awpassimov.png" alt="">
											</div>
											<div class="product-body">
												<p class="product-category">AWP</p>
												<h3 class="product-name"><a href="#">StatTrak™ AWP / Field-Tested
													Asiimov (Field-Tested)</a></h3>
												<h4 class="product-price">$399.99 <del class="product-old-price">$400.98</del></h4>
												<div class="product-rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
												</div>
												<div class="product-btns">
													<button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button>
													<button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">add to compare</span></button>
													<button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
												</div>
											</div>
											<div class="add-to-cart">
												<button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
											</div>
										</div>
										<!-- /product -->

										<!-- product -->
										<div class="product">
											<div class="product-img">
												<img src="./img/sticketholo.png" alt="">
											</div>
											<div class="product-body">
												<p class="product-category">Sticker</p>
												<h3 class="product-name"><a href="#">Team Dignitas (Holo)</a></h3>
												<h4 class="product-price">$585.00 <del class="product-old-price">$746.09</del></h4>
												<div class="product-rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
												</div>
												<div class="product-btns">
													<button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button>
													<button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">add to compare</span></button>
													<button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
												</div>
											</div>
											<div class="add-to-cart">
												<button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
											</div>
										</div>
										<!-- /product -->
									</div>
									<div id="slick-nav-1" class="products-slick-nav"></div>
								</div>
								<!-- /tab -->
							</div>
						</div>
					</div>
					<!-- Products tab & slick -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- HOT DEAL SECTION -->
		<div id="hot-deal" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<div class="hot-deal">
							<ul class="hot-deal-countdown">
								<li>
									<div>
										<h3>02</h3>
										<span>Days</span>
									</div>
								</li>
								<li>
									<div>
										<h3>10</h3>
										<span>Hours</span>
									</div>
								</li>
								<li>
									<div>
										<h3>34</h3>
										<span>Mins</span>
									</div>
								</li>
								<li>
									<div>
										<h3>60</h3>
										<span>Secs</span>
									</div>
								</li>
							</ul>
							<h2 class="text-uppercase">hot deal this week</h2>
							<p>New Items Up to 50% OFF</p>
							<a class="primary-btn cta-btn" href="#">Shop now</a>
						</div>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /HOT DEAL SECTION -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<!-- section title -->
					<div class="col-md-12">
						<div class="section-title">
							<h3 class="title">Top selling</h3>
							<div class="section-nav">
								<ul class="section-tab-nav tab-nav">
									<li class="active"><a data-toggle="tab" href="#tab2">Knife</a></li>
									<li><a data-toggle="tab" href="#tab2">Gun</a></li>
									<li><a data-toggle="tab" href="#tab2">Gloves</a></li>
									<li><a data-toggle="tab" href="#tab2">Case</a></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /section title -->

					<!-- Products tab & slick -->
					<div class="col-md-12">
						<div class="row">
							<div class="products-tabs">
								<!-- tab -->
								<div id="tab1" class="tab-pane active">
									<div class="products-slick" data-nav="#slick-nav-1">
										<!-- product -->
										<div class="product">
											<div class="product-img">
												<img src="./img/navaja.png" alt="">
												<div class="product-label">
													<span class="sale">-16%</span>
													<span class="new">NEW</span>
												</div>
											</div>
											<div class="product-body">
												<p class="product-category">Knife</p>
												<h3 class="product-name"><a href="#">★ Navaja Knife | Marble Fade (Factory New)</a></h3>
												<h4 class="product-price">$140.00 <del class="product-old-price">$150.00</del></h4>
												<div class="product-rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
												</div>
												<div class="product-btns">
													<button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button>
													<button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">add to compare</span></button>
													<button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
												</div>
											</div>
											<div class="add-to-cart">
												<button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
											</div>
										</div>
										<!-- /product -->

										<!-- product -->
										<div class="product">
											<div class="product-img">
												<img src="./img/gammardoppler.png" alt="">
												<div class="product-label">
													<span class="sale">-26.15%</span>
													<span class="new">NEW</span>
												</div>
											</div>
											<div class="product-body">
												<p class="product-category">Knife</p>
												<h3 class="product-name"><a href="#">StatTrak™ ★ Butterfly Knife / Minimal Wear
													Gamma Doppler Emerald (Minimal Wear)</a></h3>
												<h4 class="product-price">$23 506.80 <del class="product-old-price">$17 358.87</del></h4>
												<div class="product-rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-o"></i>
												</div>
												<div class="product-btns">
													<button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button>
													<button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">add to compare</span></button>
													<button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
												</div>
											</div>
											<div class="add-to-cart">
												<button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
											</div>
										</div>
										<!-- /product -->

										<!-- product -->
										<div class="product">
											<div class="product-img">
												<img src="./img/vucal.png" alt="">
												<div class="product-label">
													<span class="sale">-6.18%</span>
												</div>
											</div>
											<div class="product-body">
												<p class="product-category">AK-47</p>
												<h3 class="product-name"><a href="#">StatTrak™ AK-47 / Minimal Wear
													Vulcan (Minimal Wear)</a></h3>
												<h4 class="product-price">$900.00 <del class="product-old-price">$959.00</del></h4>
												<div class="product-rating">
												</div>
												<div class="product-btns">
													<button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button>
													<button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">add to compare</span></button>
													<button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
												</div>
											</div>
											<div class="add-to-cart">
												<button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
											</div>
										</div>
										<!-- /product -->

										<!-- product -->
										<div class="product">
											<div class="product-img">
												<img src="./img/awpassimov.png" alt="">
											</div>
											<div class="product-body">
												<p class="product-category">AWP</p>
												<h3 class="product-name"><a href="#">StatTrak™ AWP / Field-Tested
													Asiimov (Field-Tested)</a></h3>
												<h4 class="product-price">$399.99 <del class="product-old-price">$400.98</del></h4>
												<div class="product-rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
												</div>
												<div class="product-btns">
													<button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button>
													<button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">add to compare</span></button>
													<button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
												</div>
											</div>
											<div class="add-to-cart">
												<button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
											</div>
										</div>
										<!-- /product -->

										<!-- product -->
										<div class="product">
											<div class="product-img">
												<img src="./img/sticketholo.png" alt="">
											</div>
											<div class="product-body">
												<p class="product-category">Sticker</p>
												<h3 class="product-name"><a href="#">Team Dignitas (Holo)</a></h3>
												<h4 class="product-price">$585.00 <del class="product-old-price">$746.09</del></h4>
												<div class="product-rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
												</div>
												<div class="product-btns">
													<button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button>
													<button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">add to compare</span></button>
													<button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
												</div>
											</div>
											<div class="add-to-cart">
												<button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
											</div>
										</div>
										<!-- /product -->
									</div>
									<div id="slick-nav-1" class="products-slick-nav"></div>
								</div>
								<!-- /tab -->
							</div>
						</div>
					</div>
					<!-- Products tab & slick -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->
		<!-- /SECTION -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-4 col-xs-6">
						<div class="section-title">
							<h4 class="title">Top selling</h4>
							<div class="section-nav">
								<div id="slick-nav-3" class="products-slick-nav"></div>
							</div>
						</div>

						<div class="products-widget-slick" data-nav="#slick-nav-3">
							<div>
								<!-- product widget -->
								<div class="product-widget">
									<div class="product-img">
										<img src="./img/dragon.png" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">AWP</p>
										<h3 class="product-name"><a href="#">Dragon Lore (Field-Tested)</a></h3>
										<h4 class="product-price">$6 416.73 <del class="product-old-price">$8 341.75</del></h4>
									</div>
								</div>
								<!-- /product widget -->

								<!-- product widget -->
								<div class="product-widget">
									<div class="product-img">
										<img src="./img/deprint.png" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">Desert Engle</p>
										<h3 class="product-name"><a href="#">Printstream (Factory New)</a></h3>
										<h4 class="product-price">$213.34 <del class="product-old-price">$274.33</del></h4>
									</div>
								</div>
								<!-- /product widget -->

								<!-- product widget -->
								<div class="product-widget">
									<div class="product-img">
										<img src="./img/m4assi.png" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">M4A4</p>
										<h3 class="product-name"><a href="#">Asiimov (Field-Tested)</a></h3>
										<h4 class="product-price">$259.99 <del class="product-old-price">$354.66</del></h4>
									</div>
								</div>
								<!-- product widget -->
							</div>

							<div>
								<!-- product widget -->
								<div class="product-widget">
									<div class="product-img">
										<img src="./img/m4shotrod.png" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">M4A1-S</p>
										<h3 class="product-name"><a href="#">Hot Rod (Minimal Wear)</a></h3>
										<h4 class="product-price">$1 099.98 <del class="product-old-price">$944.11</del></h4>
									</div>
								</div>
								<!-- /product widget -->

								<!-- product widget -->
								<div class="product-widget">
									<div class="product-img">
										<img src="./img/augakihaba.png" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">AUG</p>
										<h3 class="product-name"><a href="#">Akihabara Accept (Minimal Wear)</a></h3>
										<h4 class="product-price">$1 549.00<del class="product-old-price">$2 000.48</del></h4>
									</div>
								</div>
								<!-- /product widget -->

								<!-- product widget -->
								<div class="product-widget">
									<div class="product-img">
										<img src="./img/fire.png" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">AK-47</p>
										<h3 class="product-name"><a href="#">Fire Serpent (Minimal Wear)</a></h3>
										<h4 class="product-price">$1 099.99 <del class="product-old-price">$1 425.58</del></h4>
									</div>
								</div>
								<!-- product widget -->
							</div>
						</div>
					</div>

					<div class="col-md-4 col-xs-6">
						<div class="section-title">
							<h4 class="title">Top selling</h4>
							<div class="section-nav">
								<div id="slick-nav-4" class="products-slick-nav"></div>
							</div>
						</div>

						<div class="products-widget-slick" data-nav="#slick-nav-4">
							<div>
								<!-- product widget -->
								<div class="product-widget">
									<div class="product-img">
										<img src="./img/talonruby.png" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">Knife</p>
										<h3 class="product-name"><a href="#">★ Talon Knife | Doppler Ruby (Factory New)</a></h3>
										<h4 class="product-price">$5 999.00 <del class="product-old-price">$4 795.01</del></h4>
									</div>
								</div>
								<!-- /product widget -->

								<!-- product widget -->
								<div class="product-widget">
									<div class="product-img">
										<img src="./img/lore.png" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">Knife</p>
										<h3 class="product-name"><a href="#">★ M9 Bayonet | Lore (Factory New)</a></h3>
										<h4 class="product-price">$2 899.00 <del class="product-old-price">$3 942.94</del></h4>
									</div>
								</div>
								<!-- /product widget -->

								<!-- product widget -->
								<div class="product-widget">
									<div class="product-img">
										<img src="./img/flipdopper.png" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">Knife</p>
										<h3 class="product-name"><a href="#">★ Flip Knife | Doppler Sapphire (Factory New)</a></h3>
										<h4 class="product-price">$3 099.00 <del class="product-old-price">$3 904.28</del></h4>
									</div>
								</div>
								<!-- product widget -->
							</div>

							<div>
								<!-- product widget -->
								<div class="product-widget">
									<div class="product-img">
										<img src="./img/buomdopplerphase4.png" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">Knife</p>
										<h3 class="product-name"><a href="#">★ StatTrak™ Butterfly Knife | Gamma Doppler Phase 4 (Minimal Wear)</a></h3>
										<h4 class="product-price">$2 787.19 <del class="product-old-price">$3 623.35</del></h4>
									</div>
								</div>
								<!-- /product widget -->

								<!-- product widget -->
								<div class="product-widget">
									<div class="product-img">
										<img src="./img/bowiesapphire.png" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">Knife</p>
										<h3 class="product-name"><a href="#">★ StatTrak™ Bowie Knife | Doppler Sapphire (Minimal Wear)</a></h3>
										<h4 class="product-price">$2 995.58 <del class="product-old-price">$3 446.24</del></h4>
									</div>
								</div>
								<!-- /product widget -->

								<!-- product widget -->
								<div class="product-widget">
									<div class="product-img">
										<img src="./img/karadopperphase2.png" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">Knife</p>
										<h3 class="product-name"><a href="#">★ Karambit | Gamma Doppler Phase 2 (Factory New)</a></h3>
										<h4 class="product-price">$2 486.60 <del class="product-old-price">$2 860.69</del></h4>
									</div>
								</div>
								<!-- product widget -->
							</div>
						</div>
					</div>

					<div class="clearfix visible-sm visible-xs"></div>

					<div class="col-md-4 col-xs-6">
						<div class="section-title">
							<h4 class="title">Top selling</h4>
							<div class="section-nav">
								<div id="slick-nav-5" class="products-slick-nav"></div>
							</div>
						</div>

						<div class="products-widget-slick" data-nav="#slick-nav-5">
							<div>
								<!-- product widget -->
								<div class="product-widget">
									<div class="product-img">
										<img src="./img/glovespandora.png" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">Gloves</p>
										<h3 class="product-name"><a href="#">★ Sport Gloves | Pandora's Box (Battle-Scarred)</a></h3>
										<h4 class="product-price">$2 200.00 <del class="product-old-price">$1 788.67</del></h4>
									</div>
								</div>
								<!-- /product widget -->

								<!-- product widget -->
								<div class="product-widget">
									<div class="product-img">
										<img src="./img/glovesmablefade.png" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">GLoves</p>
										<h3 class="product-name"><a href="#">★ Specialist Gloves | Marble Fade (Minimal Wear)</a></h3>
										<h4 class="product-price">$1 053.34 <del class="product-old-price">$1 513.46</del></h4>
									</div>
								</div>
								<!-- /product widget -->

								<!-- product widget -->
								<div class="product-widget">
									<div class="product-img">
										<img src="./img/glovesboom.png" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">GLoves</p>
										<h3 class="product-name"><a href="#">★ Moto Gloves | Boom! (Factory New)</a></h3>
										<h4 class="product-price">$1 300.97 <del class="product-old-price">$1 496.69</del></h4>
									</div>
								</div>
								<!-- product widget -->
							</div>

							<div>
								<!-- product widget -->
								<div class="product-widget">
									<div class="product-img">
										<img src="./img/glovesfield.png" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">Gloves</p>
										<h3 class="product-name"><a href="#">★ Specialist Gloves | Field Agent (Minimal Wear)</a></h3>
										<h4 class="product-price">$1 019.00 <del class="product-old-price">$1 273.02</del></h4>
									</div>
								</div>
								<!-- /product widget -->

								<!-- product widget -->
								<div class="product-widget">
									<div class="product-img">
										<img src="./img/glovespow.png" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">Gloves</p>
										<h3 class="product-name"><a href="#">★ Moto Gloves | POW! (Minimal Wear)</a></h3>
										<h4 class="product-price">1 092.42 <del class="product-old-price">$1 420.14</del></h4>
									</div>
								</div>
								<!-- /product widget -->

								<!-- product widget -->
								<div class="product-widget">
									<div class="product-img">
										<img src="./img/glovescobalt.png" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">GLoves</p>
										<h3 class="product-name"><a href="#">★ Hand Wraps | Cobalt Skulls (Minimal Wear)</a></h3>
										<h4 class="product-price">$1 000.00 <del class="product-old-price">$1 243.05</del></h4>
									</div>
								</div>
								<!-- product widget -->
							</div>
						</div>
					</div>

				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- NEWSLETTER -->
		<div id="newsletter" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<div class="newsletter">
							<p>Sign Up for the <strong>NEWSLETTER</strong></p>
							<form>
								<input class="input" type="email" placeholder="Enter Your Email">
								<button class="newsletter-btn"><i class="fa fa-envelope"></i> Subscribe</button>
							</form>
							<ul class="newsletter-follow">
								<li>
									<a href="https://www.facebook.com/profile.php?id=100044146933366"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="https://twitter.com/s1mpleO"><i class="fa fa-twitter"></i></a>
								</li>
								<li>
									<a href="https://www.instagram.com/qagmih_/"><i class="fa fa-instagram"></i></a>
								</li>
								<li>
									<a href="https://www.pinterest.com/egriffith2083/csgo-skins/"><i class="fa fa-pinterest"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /NEWSLETTER -->
		<jsp:include page="footer.jsp"></jsp:include>

		<!-- jQuery Plugins -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/slick.min.js"></script>
		<script src="js/nouislider.min.js"></script>
		<script src="js/jquery.zoom.min.js"></script>
		<script src="js/main.js"></script>

	</body>
</html>
